import Vue from "vue";
import Vuex from "vuex";

import AuthModule from "./modules/Auth";
import InfoModule from "./modules/Info";
import ShipmentsModule from "./modules/Shipments";
import PaymentsModule from "./modules/Payments";
import AddressesModule from "./modules/Addresses";
import InvoicesModule from "./modules/Invoices";
import FinancesModule from "./modules/Finances";
import BlogModule from "./modules/Blog";
import ServiceModule from "./modules/ServiceModule";
import CdekModule from "./modules/Cdek";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const namespaced = true;

export default function() {
  // export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app: {
        namespaced,
        state: () => ({
          loading: {
            page: false,
            content: false
          },
          lang: "ru",
          _ready: false
        }),
        actions: {},
        mutations: {
          SET_LOADING: (state, loading) => {
            state.loading["content"] = loading;
          },
          SET_LANG: (state, lang) => {
            localStorage.setItem("sol-lang", lang);
            state.lang = lang;
          },
          SET_APP_READY: state => {
            state._ready = true;
          }
        },
        getters: {
          is_ready: state => state._ready
        }
      },
      auth: AuthModule,
      Shipments: ShipmentsModule,
      Payments: PaymentsModule,
      Info: InfoModule,
      Addresses: AddressesModule,
      Invoices: InvoicesModule,
      Finances: FinancesModule,
      Blog: BlogModule,
      Service: ServiceModule,
      Cdek: CdekModule
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  });

  return Store;
}
