import { $api } from "@/boot/axios";

export default {
  namespaced: true,
  state: () => ({
    checked: [],
    list: []
  }),
  actions: {
    SHIPMENTS_GET: (ctx, params) => {
      let url = `/shipping/shipment`,
        param_string = new URLSearchParams(params).toString();

      if (param_string && param_string.length) url += `?${param_string}`;

      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api.get(url);

          ctx.commit("SET_SHIPMENTS", data.shipments);
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    SHIPMENT_REMOVE: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          const request = await $api.delete(`/shipping/shipment`, {
            data: { shipment_id }
          });
          done(request);
          ctx.commit("REMOVE_FORM_VIEW", shipment_id);
        } catch (e) {
          fail(e);
        }
      });
    },
    SHIPMENT_GET_STICKERS: (ctx, { shipment_ids, print_format = "A4" }) => {
      if (!shipment_ids || !shipment_ids.length) {
        throw new Error(
          "Shipment must has Array type and have at least one shipment"
        );
        return;
      }

      return new Promise(async (done, fail) => {
        try {
          const stickers = $api.post(
            `v2/shipping/shipments/get-stickers`,
            {
              shipment_ids,
              print_format
            },
            { responseType: "arraybuffer" }
          );

          done(stickers);
        } catch (e) {
          console.log(e);
        } finally {
        }
      });
    }
  },
  mutations: {
    UPADTE_CHECKED: (state, { shipment, checked }) => {
      if (checked) state.checked.push(shipment);
      else {
        let idx = state.checked.findIndex(s => s.id === shipment.id);
        state.checked.splice(idx, 1);
      }
    },
    CLEAR_CHECKED: state => {
      state.checked = [];
    },
    SET_SHIPMENTS: (state, shipments) => {
      state.list = shipments;
    },
    REMOVE_FORM_VIEW: (state, shipment_id) => {
      let idx = state.list.findIndex(s => s.id === shipment_id);
      state.list.splice(idx, 1);
    }
  },
  getters: {
    checked_shipments: state => state.checked,
    shipments_list: state => state.list
  }
};
