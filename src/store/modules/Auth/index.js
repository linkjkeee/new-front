const ACTIONS = {
  AUTH_REQUEST: "AUTH_REQUEST",
  GET_USER: "GET_USER",
  AUTH_LOGOUT: "AUTH_LOGOUT"
};

const MUTATIONS = {
  SET_LOADING: "SET_LOADING",
  SET_TOKEN: "SET_TOKEN",
  SET_USER: "SET_USER",
  SET_CONSOLIDATION_SENDER_AVAILABLE: "SET_CONSOLIDATION_SENDER_AVAILABLE",
  AUTH_ERROR: "AUTH_ERROR"
};

const ACCESS_KEY = process.env.SELLER_API_ACCESS_KEY;
const _token_key = process.env.ECOM_USER_TOKEN_KEY;
// console.log(localStorage.getItem(_token_key));

export default {
  namespaced: true,
  state: {
    token: localStorage.getItem(_token_key) || null,
    user: null,
    loading: false,
    senderConsolidationAvailable: false,
    loading: false
  },
  getters: {
    is_logged: state => !!state.token,
    senderConsolidationAvailable: state => !!state.senderConsolidationAvailable,
    valid_profile: state => {
      let user = state.user;

      if (user && user.user_type) {
        if (user.user_type === "individual") {
          if (user.itn) {
            return true;
          }
        }
        if (user.user_type === "self_employed") {
          if (user.itn && user.company_name && user.company_name.length) {
            return true;
          }
        }
        if (user.user_type === "entity") {
          if (user.psrn) {
            return true;
          }
        }
      } else {
        return false;
      }
    },
    user: state => state.user,
    loading: state => state.loading,
    customer_vars: state => (state.user && state.user.vars) || null
  },
  actions: {
    [ACTIONS.AUTH_REQUEST]: async (
      { commit, dispatch },
      { login, password }
    ) => {
      return new Promise(async (ok, fail) => {
        commit("SET_LOADING", true);

        try {
          const {
            data: { token }
          } = await $api.post(
            "auth/web/",
            { login, password },
            {
              headers: {
                "access-key": ACCESS_KEY
              }
            }
          );

          // if (success) {
          localStorage.setItem(_token_key, token);

          commit("SET_TOKEN", token);

          await dispatch("GET_USER");

          localStorage.removeItem("sol-user-token");

          ok();
          // }
        } catch (e) {
          commit(MUTATIONS.AUTH_ERROR);
          fail(e);
        } finally {
          commit("SET_LOADING", false);
        }
      });
    },
    [ACTIONS.GET_USER]: async ({ commit, dispatch, state }) => {
      return new Promise(async (ok, fail) => {
        try {
          commit("SET_LOADING", true);
          const { data: user } = await $api.get("/user/");

          commit("SET_USER", { user });

          commit("SET_CONSOLIDATION_SENDER_AVAILABLE", {
            senderConsolidationAvailable:
              user && user.vars && parseInt(user.vars.CUSTOMER_DHL_ENABLE) === 1
          });

          // ? OR HERE ?
          await dispatch("Finances/GET_BALANCE", null, { root: true });

          ok(user);
        } catch (e) {
          fail(e);
        } finally {
          commit("SET_LOADING", false);
        }
      });
    },
    [ACTIONS.AUTH_LOGOUT]: ({ commit }) => {
      commit("SET_USER", { user: null });
      commit("AUTH_ERROR");
    }
  },
  mutations: {
    [MUTATIONS.SET_LOADING]: (state, loading) => {
      state.loading = loading;
    },
    [MUTATIONS.SET_TOKEN]: (state, token) => {
      $api.defaults.headers.common["API-KEY"] = token;
      state.token = token;
      localStorage.setItem(_token_key, token);
    },
    [MUTATIONS.SET_USER]: (state, { user }) => {
      state.user = user;
    },
    [MUTATIONS.SET_CONSOLIDATION_SENDER_AVAILABLE]: (
      state,
      { senderConsolidationAvailable }
    ) => {
      state.senderConsolidationAvailable = senderConsolidationAvailable;
    },
    [MUTATIONS.AUTH_ERROR]: state => {
      delete $api.defaults.headers.common["API-KEY"];
      state.token = null;
      state.user = null;
      localStorage.removeItem(_token_key);
    }
  }
};
