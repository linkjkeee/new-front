const ACTIONS = {
  UPLOAD_FILE: "UPLOAD_FILE"
};

export default {
  namespaced: true,
  state: () => ({}),
  actions: {
    [ACTIONS.UPLOAD_FILE]: (ctx, file) => {
      return new Promise(async (done, fail) => {
        try {
          let fd = new FormData();
          fd.append("file", file);

          const {
            data: { filename }
          } = await $api.post(`maintenance/upload`, fd);
          done(filename);
        } catch (e) {
          fail(e);
        }
      });
    }
  },
  mutations: {},
  getters: {}
};
