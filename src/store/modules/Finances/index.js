const ACTIONS = {
  GET_BALANCE: "GET_BALANCE"
};
const MUTATIONS = {
  SET_BALANCE: "SET_BALANCE"
};

export default {
  namespaced: true,
  state: () => ({
    balance: null
  }),
  actions: {
    [ACTIONS.GET_BALANCE]: ctx => {
      return new Promise(async (done, fail) => {
        try {
          const { data: balance } = await $api.get(`/user/balance`);
          ctx.commit("SET_BALANCE", balance.balance);
          done(balance);
        } catch (e) {
          fail(e);
        }
      });
    }
  },
  mutations: {
    [MUTATIONS.SET_BALANCE]: (state, balance) => {
      state.balance = balance;
    }
  },
  getters: {
    balance: state => state.balance
  }
};
