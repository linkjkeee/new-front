const ACTIONS = {
  GET_INVOICES: "GET_INVOICES",
  GET_INVOICE: "GET_INVOICE",
  PRINT_INVOICE: "PRINT_INVOICE",
  REMOVE_INVOICE: "REMOVE_INVOICE",
  MARK_AS_PAID: "MARK_AS_PAID"
};

export default {
  namespaced: true,
  state: () => ({}),
  actions: {
    [ACTIONS.GET_INVOICES]: ({ commit }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { invoices, on_page, page, total }
          } = await $api.get(`/payments/pay_money/invoices`);

          done({ invoices, on_page, page, total });
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.GET_INVOICE]: ({ commit }, invoice_id) => {
      return new Promise(async (done, fail) => {
        try {
          const req = await $api.get(
            `/payments/pay_money/invoices/${invoice_id}`
          );

          done(req);
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.PRINT_INVOICE]: ({ commit }, invoice_id) => {},
    [ACTIONS.REMOVE_INVOICE]: ({ commit }, invoice_id) => {
      return new Promise(async (done, fail) => {
        try {
          const req = await $api.delete(
            `/payments/pay_money/invoices/${invoice_id}`
          );
          done(req);
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.MARK_AS_PAID]: ({ commit }, invoice_id) => {
      return new Promise(async (done, fail) => {
        try {
          const req = await $api.put(
            `/payments/pay_money/invoices/${invoice_id}/paid`
          );
          done(req);
        } catch (e) {
          fail(e);
        }
      });
    }
  },
  mutations: {},
  getters: {}
};
