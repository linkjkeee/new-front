import { $api } from "@/boot/axios";

export default {
  state: () => ({
    payments: []
  }),
  actions: {
    PAYMENTS_GET: async (
      ctx,
      params = {
        page: 0,
        payment_type: "+",
        on_page: 5
      }
    ) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api.post(`/payments`, params);
          done(data);
          ctx.commit("PAYMENTS_SET", data.payments);
        } catch (e) {
          fail(e);
        } finally {
        }
      });
    }
  },
  mutations: {
    PAYMENTS_SET: (state, payments) => {
      state.payments = payments;
    }
  },
  getters: {
    payments_list: state => state.payments
  }
};
