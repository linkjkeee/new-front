const ACTIONS = {
  GET_RECENT_POSTS: "GET_RECENT_POSTS"
};

const MUTATIONS = {
  SET_RECENT_POSTS: "SET_RECENT_POSTS",
  SET_TIMESTAMP: "SET_TIMESTAMP"
};

export default {
  namespaced: true,
  state: () => ({
    posts: null,
    timestamp: null
  }),
  actions: {
    [ACTIONS.GET_RECENT_POSTS]: ({ state, commit }) => {
      return new Promise(async (done, fail) => {
        try {
          const currentTimestamp = new Date().getTime() / 1000;
          if (
            !state.posts ||
            !state.timestamp ||
            currentTimestamp - state.timestamp > 60 * 15 // 15 minutes
          ) {
            const data = await fetch(
              "https://ecomexpress.ru/wp-json/wp/v2/posts?per_page=4&_embed"
            );
            const rawPosts = await data.json();

            const blogPosts = rawPosts.map(post => {
              const html = post.content.rendered;
              let div = document.createElement("div");
              div.innerHTML = html;
              const text = (div.textContent || div.innerText || "").replace(
                /(\r\n|\n|\r)/gm,
                ""
              );
              const title = post.title.rendered;
              const url = post.link;
              const rawDate = new Date(post.date);
              const year = new Intl.DateTimeFormat("en", {
                year: "numeric"
              }).format(rawDate);
              const month = new Intl.DateTimeFormat("en", {
                month: "2-digit"
              }).format(rawDate);
              const day = new Intl.DateTimeFormat("en", {
                day: "2-digit"
              }).format(rawDate);
              const date = `${day}.${month}.${year}`;
              const imageData = post._embedded["wp:featuredmedia"][0];
              const media = imageData.media_details;
              const image = `https://ecomexpress.ru/wp-content/uploads/${media.file}`;
              const postId = post.id;

              return { title, url, text, date, image, postId };
            });

            commit("SET_RECENT_POSTS", blogPosts);
            commit("SET_TIMESTAMP", new Date().getTime() / 1000);
            done(blogPosts);
          } else {
            done(state.blogPosts);
          }
        } catch (e) {
          fail(e);
        }
      });
    }
  },
  mutations: {
    [MUTATIONS.SET_RECENT_POSTS]: (state, posts) => {
      state.posts = posts;
    },
    [MUTATIONS.SET_TIMESTAMP]: (state, timestamp) => {
      state.timestamp = timestamp;
    }
  },
  getters: {
    posts: state => state.posts,
    updated: state => state.timestamp
  }
};
