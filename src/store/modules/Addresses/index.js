const ACTIONS = {
  GET_ADDRESSES: "GET_ADDRESSES"
};

export default {
  namespaced: true,
  // TODO: DECIDE GLOBAL SAVE OR NOT
  state: () => ({
    addresses: null
  }),
  actions: {
    [ACTIONS.GET_ADDRESSES]: ctx => {
      return new Promise(async (done, fail) => {
        try {
          const { data: records } = await $api.get(`/v2/shipping/addresses`);

          done(records);
        } catch (e) {
          fail(e);
        }
      });
    }
  }
};
