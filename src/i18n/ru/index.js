import CommonTranslate from "./common";
import TranslateLayout from "./layout";
import PagesTranslation from "./pages";

export default {
  ...TranslateLayout,
  ...PagesTranslation,
  ...CommonTranslate
};
