// This is just an example,
// so you can safely delete all default props below

export default {
  nav: {
    shipments: {
      _parent: "Отправки",
      new_shipment: {
        title: "Создать отправку",
        caption: "Создайте заявку на отправку посылки"
      },
      shipments: {
        title: "Отправки",
        caption: "Управляйте своими заявками"
      },
      warehouse: {
        title: "Товары на складе",
        caption: "Ваши товары на складе в США"
      },
      calc: {
        title: "Калькулятор",
        caption: "стоимости доставки"
      },
      replenishment: {
        title: "Пополнить счёт",
        caption: "Пополните счёт один из удобных для вас способом"
      }
    },
    bills: {
      _parent: "Счёт",
      my_bills: {
        title: "Мой счёт",
        caption: "Сводная информация о Вашем счёте"
      },
      money_in: {
        title: "Пополнить счёт",
        caption: "Пополнить счёт одним из удобных для вас способом"
      },
      money_out: {
        title: "Вывод остатков",
        caption: "Вывод остатков в долларах удобным для вас методом"
      }
    },
    payments: {
      _parent: "Платежи",
      payments_in: {
        title: "Входящие платежи",
        caption: "Ваши полученные платежи"
      },
      payments_out: {
        title: "Отправленные платежи",
        caption: "Ваши отправленные платежи"
      }
    },
    integrations: {
      _parent: "Источники продаж",
      integrations: "интеграции",
      integrations_etsy: {
        title: "Подключить Etsy"
      },
      integrations_amazon: {
        title: "Подключить Amazon"
      },
      integrations_shopify: {
        title: "Подключить Shopify"
      },
      integrations_ebay: {
        title: "Подключить Ebay"
      }
    },
    account: {
      my_products: "Мои товары",
      personal_info: "Персональная информация",
      logout: "Выход",
      address_book: "Адресная книга",
      customs_codes_catalog: "Каталог кодов ТНВЭД"
    },
    home: "Главная"
  },
  balance: {
    waiting: "Ожидает",
    blocked: "Заблокировано",
    available: "Доступно"
  }
};
