// This is just an example,
// so you can safely delete all default props below

export default {
  nav: {
    shipments: {
      _parent: "Shipments",
      new_shipment: {
        title: "New shipment",
        caption: "Create own new shipment"
      },
      shipments: {
        title: "My shipments",
        caption: "Manage own shipments"
      },
      warehouse: {
        title: "Warehouse",
        caption: "Info about your products in US warehouse"
      },
      calc: {
        title: "Calculator",
        caption: "of shipping cost"
      }
    },
    bills: {
      _parent: "Account",
      my_bills: {
        title: "Account status",
        caption: "Check out your account summary report"
      },
      money_in: {
        title: "Account replenishment",
        caption:
          "Account replenishment in one of the most convenient ways for you"
      },
      money_out: {
        title: "Withdrawal of funds",
        caption: "Withdrawal of your funds by a convenient method for you"
      }
    },
    payments: {
      _parent: "Payments",
      payments_in: {
        title: "Incoming payments",
        caption: "Your received payments"
      },
      payments_out: {
        title: "Outcoming payments",
        caption: "Your sent payments"
      }
    },
    integrations: {
      _parent: "Sales channels",
      integrations_etsy: {
        title: "Connect Etsy"
      },
      integrations_amazon: {
        title: "Connect Amazon"
      },
      integrations_shopify: {
        title: "Connect Shopify"
      },
      integrations_ebay: {
        title: "Connect Ebay"
      }
    },
    account: {
      my_products: "My products",
      personal_info: "Personal info",
      logout: "Logout",
      address_book: "Address book",
      customs_codes_catalog: "Customs codes catalog"
    },
    home: "Home"
  },
  balance: {
    waiting: "Waiting",
    blocked: "Blocked",
    available: "Available"
  }
};
