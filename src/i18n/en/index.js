import TranslateLayout from "./layout";
import CommonTranslate from "./common";

export default { ...TranslateLayout, ...CommonTranslate };
