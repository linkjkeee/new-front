// import something here
import axios from "axios";

let API_URL;

if (process.env.DEV) {
  API_URL = process.env.LOCAL_API_URL;
  console.log(
    `%c${process.env.NODE_ENV}`,
    "color:royalblue;font-family:system-ui;font-size:3rem;font-weight:bold"
  );
} else if (process.env.PROD) {
  API_URL = process.env.PRODUCTION_API_URL;
}

export const $api = axios.create({
  baseURL: API_URL,
  timeout: 60000
});

export default async (/* { app, router, Vue ... } */ { Vue }) => {
  // window.$api = $api;
  Vue.prototype.$api = $api;
};
