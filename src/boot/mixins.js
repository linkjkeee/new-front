import FormatStrings from "../utils/stringFormatter";
import { openURL, format, copyToClipboard, exportFile } from "quasar";
const { capitalize } = format;

const EN_StringFormatter = new FormatStrings("en-US");
const RU_StringFormatter = new FormatStrings("ru-RU");

export default async ({ Vue, ...app } /* { app, router, Vue ... } */) => {
  Vue.mixin({
    methods: {
      $toUSD(val, options = { currency: "USD" }) {
        return EN_StringFormatter.toCurrency(val, options);
      },
      $toRUB(val, options = { currency: "RUB" }) {
        return RU_StringFormatter.toCurrency(val, options);
      },
      $toFixed(val, fraction = 2) {
        return RU_StringFormatter.toLocaleFixed(val, [fraction, fraction]);
      },
      $parseApiError(e) {
        let message, errors;

        message =
          (e.response && e.response.data && e.response.data.message) || null;

        let ERROR =
          (e && e.response && e.response.data && e.response.data.errors) ||
          null;

        if (ERROR) {
          if (Array.isArray(ERROR)) {
            errors = ERROR;
          } else {
            try {
              errors = Object.values(ERROR).flat();
            } catch (e) {
              console.log("On trying destruct 'object' error:", e);
            }
          }
        } else {
          errors = ERROR;
        }

        return {
          message,
          errors
        };
      },
      $showApiError(e, { fallbackMessage = null, level = "danger" } = {}) {
        const { message, errors } = this.$parseApiError(e);

        this.$alert({
          level,
          errors,
          ...((!!message && {
            title: message,
            errors
          }) || {
            message: e && e.toString(),
            title: fallbackMessage || "Ошибка сервера",
            errors
          })
        });
      },
      $capitalize(str) {
        return capitalize(str);
      },
      $openURL(params) {
        openURL(params);
      },
      $isNumber: function(evt) {
        evt = evt ? evt : window.event;
        var charCode = evt.which ? evt.which : evt.keyCode;
        if (
          charCode > 31 &&
          (charCode < 48 || charCode > 57) &&
          charCode !== 46
        ) {
          evt.preventDefault();
        } else {
          return true;
        }
      },
      $copyToClipboard(data) {
        return copyToClipboard(data);
      },
      $downloadFile(filename = "document", data) {
        exportFile(filename, data);
      }
    }
  });

  Vue.filter("formatDate", value => {
    let DATE;

    try {
      DATE = new Date(value).toLocaleDateString();
    } catch (e) {
      console.log(e);
      return value;
    }

    return DATE;
  });
};
