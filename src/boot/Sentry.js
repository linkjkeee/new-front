// Project Settings -> General Settings -> Allowed Domains
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

const IS_DEV = process.env.DEV;

if (IS_DEV) {
  console.log("IS_DEV:", IS_DEV);
  console.log("Init Sentry");
}

export default async ({ app, store, Vue }) => {
  if (!IS_DEV) {
    Sentry.init({
      Vue,
      dsn:
        "http://8a5e148672224f8682a9237c36808818@backup.seller-online.com:9000/9",
      integrations: [
        // new Integrations.Vue(),
        new Integrations.BrowserTracing()
      ],
      tracesSampleRate: 1.0,
      environment: process.env.NODE_ENV,
      tracingOptions: {
        trackComponents: true
      }
      // autoSessionTracking: false // ?????
    });
    Vue.config.logErrors = true;
    Vue.config.errorHandler = function(e = "Unknown error", vm, info) {
      console.log("@sentry catch error:", e, vm, info);

      Sentry.setContext("JS_INFO", {
        info,
        vm
      });

      Sentry.captureException(new Error(e));
    };
  }
};
