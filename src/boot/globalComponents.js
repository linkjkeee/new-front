import VueModal from "vue-js-modal";
import Tip from "@/components/Tip";
import WithLoading from "../components/_ui/WithLoading";
import PageContainer from "../components/_ui/PageContainer";
import DialogWrapper from "../components/_ui/DialogWrapper/index.vue";
import WithTransition from "../components/_ui/WithTransition";
import CustomsCodes from "../components/CustomsCodes";
import AppBanner from "../components/_ui/Banner";
import AppList from "../components/_ui/List";
import FlagIcon from "../components/_ui/Flags";
import AppInput from "../components/_ui/TextInput";
import AppButton from "../components/_ui/Button";
import LazyImage from "../components/_ui/LazyImage";

export default ({ Vue }) => {
  // ! FALLBACK DEV LOADER PLACEHOLDER
  Vue.component("Loader", {
    props: ["loading"],
    template: `<div v-if="loading">FALLBACK LOADING...</div>`
  });
  // TODO: REPLACE BY true loader

  Vue.use(VueModal, {
    injectModalsContainer: true,
    dialog: true,
    dynamic: true
  });

  Vue.component("Tip", Tip);
  Vue.component("with-loading", WithLoading);
  Vue.component("with-transition", WithTransition);
  Vue.component("PageContainer", PageContainer);
  Vue.component("DialogWrapper", DialogWrapper);
  Vue.component("customs-codes", CustomsCodes);
  Vue.component("app-banner", AppBanner);
  Vue.component("app-list", AppList);
  Vue.component("app-flag", FlagIcon);
  Vue.component("app-input", AppInput);
  Vue.component("app-btn", AppButton);
  Vue.component("lazy-img", LazyImage);
};
