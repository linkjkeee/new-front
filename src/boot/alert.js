// import Banner from "../components/_ui/Alert";
import Store from "../store";

import { Dialog } from "quasar";

import Panel from "../components/_ui/Alert/LightStyle.vue";

export default async ({ Vue } /* { app, router, Vue ... } */) => {
  Vue.mixin({
    methods: {
      $alert({
        title = null,
        message = null,
        level = "primary",
        width = 490,
        name = "_alert",
        actions = null,
        errors = null,
        closeText = "Закрыть",
        nextText = "Продолжить"
      }) {
        return new Promise(function(done, fail) {
          Dialog.create({
            component: Panel,
            // * ALERT PROPS
            title,
            message,
            level,
            actions,
            errors,
            closeText,
            nextText
          }).onDismiss(() => {
            done();
          });
        });
      },
      $confirm({
        actions,
        name = "_confirm",
        width = 560,
        is_confirm = true,
        ...content_options
      }) {
        if (!actions || !actions.length) {
          actions = [
            {
              label: "Отмена",
              value: false
            },
            {
              label: "Продолжить",
              value: true
            }
          ];
        }

        return new Promise((ok, fail) => {
          Dialog.create({
            component: Panel,
            actions,
            is_confirm,
            ...content_options
          })
            .onOk(answ => {
              ok(answ);
            })
            .onCancel(code => {
              fail(null);
            });
        });
      }
    }
  });
};
