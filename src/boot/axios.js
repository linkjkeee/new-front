import Vue from "vue";
import axios from "axios";

const ACCESS_KEY = process.env.SELLER_API_ACCESS_KEY;
let API_URL;

if (process.env.DEV) {
  // API_URL = process.env.LOCAL_API_URL;
  API_URL = process.env.DEDICATED_API_URL;

  console.log(
    `%c${process.env.NODE_ENV}`,
    "color:royalblue;font-family:system-ui;font-size:3rem;font-weight:bold"
  );
} else if (process.env.PROD) {
  API_URL = process.env.PRODUCTION_API_URL;
}

export const $api = axios.create({
  baseURL: API_URL,
  timeout: 60000
});

export default ({ Vue }) => {
  window.$api = $api;
  Vue.prototype.$api = $api;
  Vue.prototype.$axios = axios;
};
