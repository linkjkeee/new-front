export default [
  {
    tlabel: "nav.shipments._parent",
    section: "SHIPMENTS",
    childrens: [
      {
        title: "nav.shipments.new_shipment.title",
        icon: "ion-cube",
        desc: "nav.shipments.new_shipment.caption",
        href: "/shipments/create"
      },
      {
        title: "nav.shipments.shipments.title",
        icon: "ion-list",
        desc: "nav.shipments.shipments.caption",
        href: "/shipments/list"
      },
      {
        title: "Отправки СДЭК",
        icon: "img/logos/cdek.svg",
        desc: "Список созданных заявок СДЭК",
        href: "/cdek"
      },
      {
        title: "nav.shipments.warehouse.title",
        desc: "nav.shipments.warehouse.caption",
        icon: "widgets",
        href: "/products?warehouse=true"
      },
      {
        title: "nav.shipments.calc.title",
        desc: "nav.shipments.calc.caption",
        icon: "calculate",
        href: "/calculator"
      }
    ]
  },
  {
    label: "Возвраты",
    section: "RETURNS",
    childrens: [
      {
        title: "Мои возвраты",
        icon: "ion-return-left",
        desc: "Управление Вашими возвратами",
        href: "/shipments/returns"
      },
      {
        title: "Неопознанные возвраты",
        icon: "live_help",
        desc: "",
        href: "/shipments/returns/unidentified"
      }
    ]
  },
  {
    tlabel: "nav.bills._parent",
    section: "INV",
    childrens: [
      {
        title: `nav.bills.money_in.title`,
        icon: `ion-add`,
        desc: `nav.bills.money_in.caption`,
        href: `/money-in`
      },
      {
        title: "Выписанные счета",
        icon: "receipt_long",
        desc: "Список выписанных счетов",
        href: "/invoices"
      },
      {
        title: "Журнал баланса",
        icon: "manage_search",
        href: "/account/balance-log",
        desc: "История изменения Вашего счёта EcomExpress"
      }
    ],
    footer: {
      icon: "receipt_long",
      content:
        "Сделайте интеграцию с Вашими источниками продаж и оформляйте отправки в один клик! Подробности",
      badge: "NEW"
    }
  },
  {
    tlabel: "nav.integrations._parent",
    section: "INTEGRATIONS",
    childrens: [
      {
        title: "nav.integrations.integrations_etsy.title",
        icon: "img/logos/etsy.svg",
        href: "/connect/etsy"
      },
      {
        title: "nav.integrations.integrations_amazon.title",
        icon: "img/logos/amazon.svg",
        href: "/connect/amazon"
      },
      {
        title: "nav.integrations.integrations_shopify.title",
        icon: "img/logos/shopify.svg",
        href: "/connect/shopify"
      },
      {
        title: "nav.integrations.integrations_ebay.title",
        icon: "img/logos/ebay.svg",
        href: "/connect/ebay"
      }
    ]
  }
];
