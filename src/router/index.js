import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function({ store, router } /* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
  });

  Router.beforeResolve(async (to, from, next) => {
    // Router.beforeEach((to, from, next) => {

    if (
      ["AUTH_LOGIN", "AUTH_REGISTER"].includes(to.name) &&
      store.getters["auth/is_logged"]
    ) {
      next("/");
    } else {
      if (!!to.matched.some(record => record.meta.requireAuth)) {
        if (!store.getters["auth/is_logged"]) {
          next({ path: "/login", query: { cameFrom: to.fullPath } });
        } else {
          if (!!to.meta.validate_account) {
            // console.log(store.getters["auth/valid_profile"]);

            if (!store.getters["auth/user"]) {
              await store.dispatch("auth/GET_USER");
            }

            if (store.getters["auth/valid_profile"]) {
              next();
            } else {
              next({
                name: "ACCOUNT",
                params: {
                  message:
                    "Для продолжения работы с сайтом требуется актуализировать данные"
                }
              });
            }
          } else {
            next();
          }
        }
      } else {
        next();
      }
    }
  });

  return Router;
}
