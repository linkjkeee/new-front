import Store from "../store";

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        name: "HOME",
        path: "/",
        component: () => import("pages/Index.vue"),
        redirect: "/dashboard"
      },
      {
        name: "AUTH_LOGIN",
        path: "/login",
        component: () =>
          import(/* webpackChunkName: "auth" */ "pages/Auth/Login.vue")
      },
      {
        name: "AUTH_REGISTER",
        path: "/register",
        component: () =>
          import(/* webpackChunkName: "auth" */ "pages/Auth/Registration.vue")
      },
      {
        name: "DASHBOARD",
        path: "/dashboard",
        component: () => import("pages/Dashboard"),
        meta: { requireAuth: true }
      },
      {
        name: "SETTINGS_CHANGE_PASSWORD",
        path: "/settings/change-password",
        component: () => import("pages/Settings/ChangePassword/index.vue")
      },
      {
        name: "SHIPMENTS_LIST",
        path: "/shipments/list",
        component: () =>
          import(/* webpackChunkName: "shipments" */ "pages/Shipments/List"),
        props: { create_ttn: false },
        meta: {
          requireAuth: true,
          validate_account: true,
          section: "SHIPMENTS"
        }
      },
      {
        name: "CREATE_SHIPMENT",
        path: "/shipments/create",
        component: () =>
          import(/* webpackChunkName: "shipments" */ "pages/Shipments/Create"),
        meta: {
          requireAuth: true,
          validate_account: true,
          section: "SHIPMENTS"
        }
      },
      {
        name: "MY_ADDRESSES",
        path: "/addresses",
        component: () =>
          import(/* webpackChunkName: "addresses" */ "pages/Addresses"),
        meta: { requireAuth: true }
      },
      {
        name: "NEW_ADDRESS",
        path: "/addresses/new",
        component: () =>
          import(
            /* webpackChunkName: "addresses" */ "pages/Addresses/NewAddress.vue"
          ),
        meta: { requireAuth: true }
      },
      {
        name: "ACCOUNT",
        path: "/account",
        component: () =>
          import(/* webpackChunkName: "account" */ "pages/Account"),
        meta: { requireAuth: true }
      },
      {
        name: "ACCOUNT_EDIT",
        path: "/account/edit",
        component: () =>
          import(/* webpackChunkName: "account" */ "pages/Account"),
        props: { edit: true },
        meta: { requireAuth: true }
      },
      {
        name: "WAREHOUSE",
        path: "/products",
        component: () => import("pages/Warehouse"),
        meta: { requireAuth: true }
      },
      {
        name: "CONNECT_SHOP",
        path: "/connect/:platform",
        component: () => import("pages/Connect"),
        meta: { requireAuth: true, section: "INTEGRATIONS" }
      },
      {
        name: "MONEY_IN",
        path: "/money-in",
        component: () => import("pages/Money/In"),
        meta: { requireAuth: true, section: "INV" }
      },
      {
        name: "MONEY_IN_CHECKOUT",
        path: "/money-in/checkout",
        component: () => import("pages/Money/In/Checkout"),
        meta: { requireAuth: true, section: "INV" }
      },
      {
        name: "INVOICES",
        path: "/invoices",
        component: () =>
          import(/* webpackChunkName: "invoices" */ "pages/Invoices"),
        meta: { requireAuth: true, section: "INV" }
      },
      {
        name: "INVOICE_ITEM",
        path: "/invoices/:id",
        component: () =>
          import(/* webpackChunkName: "invoices" */ "pages/Invoices/Details"),
        meta: { requireAuth: true, section: "INV" }
      },
      {
        name: "NOTIFICATIONS_CENTER",
        path: "/notifications",
        component: () => import("pages/Notifications"),
        meta: { requireAuth: true }
      },
      {
        name: "RETURNS",
        path: "/shipments/returns",
        component: () =>
          import(/* webpackChunkName: "returns" */ "pages/Returns"),
        meta: { requireAuth: true, section: "RETURNS" }
      },
      {
        name: "UNIDENTIFIED_RETURNS",
        path: "shipments/returns/unidentified",
        component: () =>
          import(/* webpackChunkName: "returns" */ "pages/UnidentifiedReturns"),
        meta: {
          requireAuth: true,
          section: "RETURNS"
        }
      },
      {
        name: "CLAIM_UNIDENTIFIED_RETURN",
        path: "shipments/returns/unidentified/:return_id/claim",
        component: () =>
          import(
            /* webpackChunkName: "returns" */ "pages/UnidentifiedReturns/ReturnClaim.vue"
          ),
        meta: { requireAuth: true, section: "RETURNS" }
      },
      {
        name: "HS_CODES_CATALOG",
        path: "/hs-codes",
        component: () => import("pages/HsCodes")
      },
      {
        name: "LEGAL_INFO",
        path: "/legal-info",
        component: () => import("pages/LegalInfo")
      },
      {
        name: "LEGAL_INFO_DOCUMENT",
        path: "/legal-info/:document",
        component: () => import("pages/LegalInfo/document.vue")
      },
      {
        name: "CALCULATOR",
        path: "/calculator",
        component: () => import("pages/Calculator")
      },
      {
        name: "CDEK_LIST",
        path: "/cdek",
        component: () => import("pages/Cdek/index.vue"),
        meta: { requireAuth: true, section: "SHIPMENTS" }
      },
      {
        name: "CDEK_CREATE",
        path: "/cdek/create",
        component: () => import("pages/Cdek/Create/index.vue"),
        meta: { requireAuth: true, section: "SHIPMENTS" }
      },
      {
        name: "CDEK_CALCULATE",
        path: "/cdek/calculate",
        component: () => import("pages/Cdek/Calculate/index.vue"),
        meta: { requireAuth: true, section: "SHIPMENTS" }
      },
      {
        name: "CDEK_REQUEST_COURIER_PICKUP",
        path: "/cdek/request-courier-pickup",
        component: () => import("pages/Cdek/CourierRequest.vue"),
        meta: { requireAuth: true, section: "SHIPMENTS" }
      },
      {
        name: "CUSTOMER_DIRECT_REQUEST",
        path: "/account/customer-direct-request",
        component: () => import("pages/Account/CustomerDirect/index.vue"),
        meta: { requireAuth: true }
      },
      {
        name: "CUSTOMER_BALANCE_LOG",
        path: "/account/balance-log",
        meta: { requireAuth: true },
        component: () => import("pages/Account/BalanceLog/index.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    name: "404",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
